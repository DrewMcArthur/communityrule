---
layout: page
title: Book
permalink: /book/
---

<a href="{% link assets/book/gov-booklet-MASTER.pdf %}" style="float:right; width:30%; margin: 0 0 1em 2em; text-align:center">
    <img src="{% link assets/book/communityrules-cover.png %}" alt="Community Rules cover" style="box-shadow: 0 2px 8px #0003;" />
    <small>Download PDF</small>
</a>

Creating communities has never been easier. Online social networks enable groups to form among people who might never otherwise meet—across borders, even within neighborhoods, and around common causes that might otherwise remain isolated and underground. Getting involved in a community can be as easy as pressing “join.” But the technology can make community appear easier than it really is. Groups form, grow, and begin to thrive before they can consider how they will share power and deal with the conflicts that inevitably arise. Because many corporate social media platforms benefit from bottomless argument and conflict—it feeds more data about us to advertisers, after all—they are not designed to support problem solving and good governance.

Community Rules is a simple tool to help make great communities even better and healthier. It includes nine templates for organizational structures that communities can choose from, combine, or react against. The templates here are not meant to represent a complete set of possible arrangements. They are a provisional set to help spur much broader explorations, and to invite critique. Each of the templates includes example practitioners to illustrate how groups use that organizational structure. These templates are also available [here on CommunityRule](https://communityrule.info/templates/), where you can customize them according to the needs of your group.

<a href="{% link assets/book/gov-booklet-MASTER.pdf %}" class="pushButton">Download PDF</a>

[Print Instructions](#print-instructions) &bull; [Posters](#posters)

![Community Rules book open to Consensus spread]({% link assets/book/community-rule-book-open.jpg %})

## Print Instructions

The printable PDF below is arranged in spreads that can be stacked together to form a booklet. You'll need a printer that can print on both sides (or print every other page then flip that stack over and print the backs).

<a href="{% link assets/book/gov-booklet-MASTER-print.pdf %}" class="pushButton">Download Printable PDF</a>

----

## Posters

Check back soon for printable posters of each template spread.
