---
layout: vue
permalink: /templates/jury
rule:
  ruleID: jury
  timestamp: 2021.3.15 3:27:1 UTC
  icon: /assets/elements/icon_ju.svg
  name: Jury
  lineage: ''
  summary: Proposals are shaped and decided on by randomly selected juries.
  creator:
    name: Media Enterprise Design Lab
  modules:
    - moduleID: values
      name: Values
      icon: null
      summary: Equality, participation, study
      modules: []
    - moduleID: membership
      name: Membership
      icon: null
      summary: Juries set policies for membership and removal.
      modules: []
    - moduleID: legislature
      name: Legislature
      icon: null
      summary: >-
        All participants have the right to initiate proposals, sign them, and
        serve on juries.
      modules:
        - moduleID: sortition
          name: Sortition
          icon: null
          summary: Temporary juries form by a random selection.
          modules: []
        - moduleID: petition
          name: Petition
          icon: null
          summary: >-
            If a certain percentage of participants signs a proposal, a jury is
            formed to study it, revise it, and agree on it unanimously. 
          modules: []
        - moduleID: policy-register
          name: Policy Register
          icon: null
          summary: A proposal agreed on by a jury becomes binding for the group.
          modules: []

---

Jury
====

Proposals are shaped and decided on by randomly selected juries.

*   **Values**: Equality, participation, study
*   **Membership**: Juries set policies for membership and removal.
*   **Legislature**: All participants have the right to initiate proposals, sign them, and serve on juries.
    *   **Sortition**: Temporary juries form by a random selection.
    *   **Petition**: If a certain percentage of participants signs a proposal, a jury is formed to study it, revise it, and agree on it unanimously.
    *   **Policy Register**: A proposal agreed on by a jury becomes binding for the group.

* * *

Created By
----------

Media Enterprise Design Lab