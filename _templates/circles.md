---
layout: vue
permalink: /templates/circles
rule:
  ruleID: circles
  timestamp: 2021.3.15 3:27:1 UTC
  icon: /assets/elements/icon_cr.svg
  name: Circles
  lineage: ''
  summary: >-
    Units called Circles have the ability to decide and act on matters in their
    domains, which their members agree on through a Council.
  creator:
    name: Media Enterprise Design Lab
  modules:
    - moduleID: values
      name: Values
      icon: null
      summary: Decentralization, delegation, trust
      modules: []
    - moduleID: membership
      name: Membership
      icon: null
      summary: >-
        To join, a person must be welcomed into a particular Circle according to
        its policies.
      modules: []
    - moduleID: council
      name: Council
      icon: null
      summary: >-
        Representatives of Circles regularly meet in a Council to coordinate
        efforts and determine the domains of each Circle, as well as to add or
        remove Circles from the Council.
      modules: []
    - moduleID: delegation
      name: Delegation
      icon: null
      summary: >-
        A Circle can create roles for its members and assign authority over
        specified sub-domains.
      modules: []
    - moduleID: lazy-consensus
      name: Lazy consensus
      icon: null
      summary: >-
        Circles and the Council use consent to make decisions. Consent means that
        nobody presents a serious objection to a proposal.
      modules: []

---

Circles
=======

Units called Circles have the ability to decide and act on matters in their domains, which their members agree on through a Council.

*   **Values**: Decentralization, delegation, trust
*   **Membership**: To join, a person must be welcomed into a particular Circle according to its policies.
*   **Council**: Representatives of Circles regularly meet in a Council to coordinate efforts and determine the domains of each Circle, as well as to add or remove Circles from the Council.
*   **Delegation**: A Circle can create roles for its members and assign authority over specified sub-domains.
*   **Lazy consensus**: Circles and the Council use consent to make decisions. Consent means that nobody presents a serious objection to a proposal.

* * *

Created By
----------

Media Enterprise Design Lab