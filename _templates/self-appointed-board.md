---
layout: vue
permalink: /templates/self-appointed-board
rule:
  ruleID: self-appointed-board
  timestamp: 2021.3.15 3:27:1 UTC
  icon: /assets/elements/icon_sb.svg
  name: Self-Appointed Board
  lineage: ''
  summary: >-
    A board that selects its own members determines policies and organizes their
    implementation.
  creator:
    name: Media Enterprise Design Lab
  modules:
    - moduleID: values
      name: Values
      icon: null
      summary: Delegation, meritocracy, servant leadership
      modules: []
    - moduleID: membership
      name: Membership
      icon: null
      summary: The board sets policies for membership and removal.
      modules: []
    - moduleID: board
      name: Board
      icon: null
      summary: >-
        A board is responsible for making decisions and implementing them,
        including by delegating necessary work to others. The board elects its own
        members.
      modules:
        - moduleID: majority-voting
          name: Majority Voting
          icon: null
          summary: The board makes decisions by majority vote.
          modules: []

---

Self-Appointed Board
====================

A board that selects its own members determines policies and organizes their implementation.

*   **Values**: Delegation, meritocracy, servant leadership
*   **Membership**: The board sets policies for membership and removal.
*   **Board**: A board is responsible for making decisions and implementing them, including by delegating necessary work to others. The board elects its own members.
    *   **Majority Voting**: The board makes decisions by majority vote.

* * *

Created By
----------

Media Enterprise Design Lab