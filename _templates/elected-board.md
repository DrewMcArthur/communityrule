---
layout: vue
permalink: /templates/elected-board
rule:
  ruleID: elected-board
  timestamp: 2021.3.15 3:27:1 UTC
  icon: /assets/elements/icon_eb.svg
  name: Elected Board
  lineage: ''
  summary: An elected board determines policies and organizes their implementation.
  creator:
    name: Media Enterprise Design Lab
  modules:
    - moduleID: values
      name: Values
      icon: null
      summary: Delegation, representation, servant leadership
      modules: []
    - moduleID: membership
      name: Membership
      icon: null
      summary: The board sets policies for membership and removal.
      modules: []
    - moduleID: board
      name: Board
      icon: null
      summary: >-
        A board is responsible for making decisions and implementing them,
        including by delegating necessary work to others.
      modules:
        - moduleID: representation
          name: Representation
          icon: null
          summary: >-
            Any community participant can be a nominee for board positions. In
            regularly scheduled elections, the nominees with the largest number of
            votes become board members.
          modules: []
        - moduleID: majority-voting
          name: Majority Voting
          icon: null
          summary: The board makes decisions by majority vote.
          modules: []

---

Elected Board
=============

An elected board determines policies and organizes their implementation.

*   **Values**: Delegation, representation, servant leadership
*   **Membership**: The board sets policies for membership and removal.
*   **Board**: A board is responsible for making decisions and implementing them, including by delegating necessary work to others.
    *   **Representation**: Any community participant can be a nominee for board positions. In regularly scheduled elections, the nominees with the largest number of votes become board members.
    *   **Majority Voting**: The board makes decisions by majority vote.

* * *

Created By
----------

Media Enterprise Design Lab