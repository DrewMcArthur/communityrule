---
layout: guide
title: "Add a Rule to a software project's Git repository"
permalink: /guides/git-repo/
---

Free/open-source software projects typically have a series of files that contain basic information about how the project works. Platforms like GitHub and GitLab encourage this. For instance, `README` serves as a kind of landing page for users and potential contributors; `LICENSE` specifies the legal terms under which the software is made available; `CONTRIBUTING` explains how contributors should submit changes to the project. Likewise, a `GOVERNANCE` file can define how the project is governed.

**CommunityRule makes it easy to include governance information in a software repository.** There are two ways to do it:

**Method 1: Export a Markdown file of your Rule and add it.** Once you've created your Rule on CommunityRule, you can download it and then put it in your respository.

* At the bottom of the page where you have authored your Rule, press the "Download" button
* Depending on your browser's settings, pressing the button should download the file and either open it or ask you where you would like to save it
* Find the file on your computer and rename it to `GOVERNANCE.md`
* For easier reading, remove the YAML metadata at the bottom; delete everything beginning with `\`\`\``
* Place the file in the main directory of your project, either by uploading to GitHub/Lab or adding it to Git locally:

<pre>
mv [file_location]/GOVERNANCE.md [project_location]/
cd [project_location]
git add GOVERNANCE.md
git commit -am "Added GOVERNANCE.md, derived from CommunityRule"
</pre>


**Method 2: Publish your Rule on CommunityRule and link to it.** This way, you're not hosting the Rule directly in your repository but allowing CommunityRule to host it for you.

* Follow the [Guide on how to publish a Rule]({% link _guides/publish_rule.md %})
* In the README.md file for your project, add a line like the following:

<pre>
[Read our Rule]([URL of published Rule]) to learn how this project is governed. [![CommunityRule derived](https://communityrule.info/assets/CommunityRule-derived-000000.svg)](https://communityrule.info)
</pre>

Whichever approach you take, **make sure that your Rule is accessible to any participant in your project.**

Finally, **make a place for any other governance documentation you'll need**. We recommend having a Rule that is distinct from the details of its current implementation---separating the code from the data, in software-speak. For instance, you might make space at the bottom of your GOVERNANCE.md file for listing which community members hold which roles, or which policies your group has agreed on through processes defined in the Rule.

Congratulations on making your project's governance practices clear and transparent!
