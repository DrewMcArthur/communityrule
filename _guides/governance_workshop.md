---
layout: guide
title: "Governance workshop"
permalink: /guides/governance-workshop/
---

One of the most common ways of using CommunityRule is in facilitated workshops. This is a script of how our team has conducted these workshops with a variety of communities—mutual aid groups, governments, startups, artist collectives, and more. The facilitator of the workshop should have at least some familiarity with CommunityRule and its built-in templates. This guide is addressed to the facilitator.

Workshops have typically been conducted via videoconference using screensharing, generally with fewer than 10 participants.

The goal of the workshop is to introduce participants to the practice of governance design, focusing on both describing current experience and envisioning future goals.

**Introduce the tool and demonstrate how it works.** Describe what CommunityRule is and what it can do. Demonstrate both the Create screen and the templates, showing how to fork and edit a template. Explain the Library and show participants an example of a well-constructed Rule there. Show how to choose modules, design custom modules, edit their specifications, and nest them inside each other.

**Reflect a bit on the importance of articulating community governance.** For instance, discuss the idea of "[The Tyranny of Structurelessness](https://www.jofreeman.com/joreen/tyranny.htm)" and the need for explicit "democratic structuring." Also be sure to stress that most governance, day to day, typically occurs through cultural processes such as values and norms; these are just as important as more abstract structures.

**Explain the publication process.** Make clear that participants may publish their creations publicly to the Library, but they are not obligated to. Make sure they understand that the Library is still an experimental feature and deleting a Rule requires contacting the administrators.

**Exercise 1: Describe the status quo.** Invite participants to depict the current state of governance in a relevant community with CommunityRule. If all participants are part of one community, they should depict it as they perceive it. Otherwise, they could be asked to choose any community that is important to them, particularly one without explicit governance structures. Have participants turn off audio and video so they can focus for about 10-15 minutes.

**Exercise 1: Report-backs.** Invite participants to share their Rules. An easy way to do this is to have them screen-share their Rules and talk through what they created. They could also do so only verbally. Especially if they are all describing the same community, this is a good opportunity to reflect on the differences of their perceptions.

**Take a break!**

**Exercise 2: Envision an ideal future.** Have participants use CommunityRule again to craft a governance model for their community perhaps five years out, or another organizationally relevant timeframe.

**Exercise 2: Report-backs.** Again, invite participants to share their Rules. Have them explain their rationale for the major choices. Afterward, compare the proposals and identify patterns that emerge.

**Reflections on the process and next steps.** If participants are part of a common community, have them identify how they will continue the discussion about governance evolution. Here, the facilitator may offer some reflections on governance best practices based on past experience and the themes that arose in the workshop. They may also invite feedback on the process and CommunityRule itself. Please [share feedback back to the developers](mailto:medlab@colorado.edu)!
