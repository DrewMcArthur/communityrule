---
layout: module
title: Council
permalink: /modules/council/
summary: Member groups meet together for coordination and collective decision-making.
type: structure
---
