---
layout: module
title: Disapproval voting
permalink: /modules/disapproval_voting/
summary: Votes are cast for the options voters do not want, and the option with the fewest votes wins.
type: decision
---

or, Negative voting
