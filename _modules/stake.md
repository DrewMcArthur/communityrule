---
layout: module
title: Stake
permalink: /modules/stake/
summary: Participants contribute something of value to indicate commitment.
type: culture
---
