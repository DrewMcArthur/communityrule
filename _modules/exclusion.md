---
layout: module
title: Exclusion
permalink: /modules/exclusion/
summary: A participant may be removed from, or prevented from entering, the group or a process.
type: process
---
