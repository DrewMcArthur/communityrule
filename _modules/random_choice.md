---
layout: module
title: Random Choice
permalink: /modules/random_choice/
summary: A choice among options is made at random.
type: decision
---
