---
layout: module
title: Caucus
permalink: /modules/caucus/
summary: A semi-formal group of people organized around shared priorities.
type: structure
---

A caucus refers to the meeting of a group of people to determine shared priorities. Common connotations include:

* Members of a political party may meet to discuss issues, lobby, or narrow down a pool of candidates
* Members of a legislature may meet to decide upon an agenda based on shared goals, such as the Congressional Hispanic Caucus in the US Congress

**Input:** an engaged and informed community with shared concerns

**Output:** a meeting of members of a particular group to achieve a specific goal based on common interests

## Background

The modern history of the caucus in the United States dates back to 1763, when John Adams used the term in a journal entry to refer to the process of pre-selecting candidates; this is the first known use of the word in reference to the modern American meaning, though the process was not a public one at the time. New Zealand began using the term in the 1890s to refer to members of parliament while the UK adopted the term in the late 19th century to refer to negatively connoted systems of control.

## Feedback loops

### Sensitivities

* Can spark productive conversation and informed discussion of important topics 
* Can lead to consensus and concrete plan for addressing concerns
* May engage diverse members of society in decision-making processes
* Can provide a space for niche communities with particular missions

### Oversights

* Public caucusing may have low turnout, leading to decision-making based on the opinions and views of only a portion of the population
* It can be a time-consuming and costly process due to intensive organizational needs
* Caucusing may lack anonymity; participants may have to make public declaration of their opinions

## Implementations

### Communities

* Some states use caucus procedures to choose presidential candidates or other elected officials, such as Iowa and Maine
* The Black Caucus, Hispanic Caucus, and Out of Iraq Caucus are but a few congressional caucuses that share values, goals, and visions
* Other groups use caucusing as a means of organizing such as the Global Nursing Caucus or LGBTQ Caucus

### Tools

* Online resources of flyers, information, and procedures exist for interest groups looking to form a caucus, usually provided by national organizations or pre-existing large-scale caucuses

## Further resources

* Masters, J. & Ratnam, G. (2016). “The U.S. Presidential Nominating Process.” Council on Foreign Relations.
* Panagopoulos, C. (2010). Are Caucuses Bad for Democracy?. Political Science Quarterly, 125: 425-442. doi:10.1002/j.1538-165X.2010.tb00680.x
* Redlawsk, D. P., Tolbert, C. J., & Donovan, T. (2011). Why Iowa?: how caucuses and sequential elections improve the presidential nominating process.      University of Chicago Press.

