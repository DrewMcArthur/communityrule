---
layout: module
title: Identity
permalink: /modules/identity/
summary: How participants represent themselves and are seen by the community.
type: structure
---
